﻿using FlexCel.XlsAdapter;
using FTD2XX_NET;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace UV1240_Exporter {
  class Program {
    public int timeOutInMs = 20000;
    public byte terminator = 26; // terminator byte ends the transmission and data reading
    public String cableSerial = "SPEKKARI";
    public String outdir = AppDomain.CurrentDomain.BaseDirectory;
    public int columnCount = 2;

    FTDI port = new FTDI();
    List<double>[] data;
    String sData = "";
    AutoResetEvent rxEvent = new AutoResetEvent(false);
    DateTime timeStamp;
    XlsFile excelFile;
    bool savePDF = true;

    /// <summary>
    /// Parse raw string data to double lists.
    /// </summary>
    void ParseData() {
      data = null;
      String[] lines = sData.Split(new String[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries);
      if (lines == null || lines.Length == 0)
        throw new Exception("Invalid data (no line separators).");
      lines.ToList().ForEach(line => {
        String[] values = Regex.Split(line, "\\s+").Where(s => s.Length > 0).ToArray(); //line.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
        if (data == null) {
          data = new List<double>[columnCount]; //values.Length];
          for (int i = 0; i < data.Length; i++) {
            data[i] = new List<double>();
          }
        }
        // Special case check for occasinal zero values in the stream between x and y values.
        if (values.Length == 3 && values[1] == "0") {
          values[1] = values[2];
          values = new String[] {values[0], values[1]};
        }
        else if (data.Length != values.Length)
          throw new Exception("Invalid data (different number of values per line)");
        for (int i = 0; i < values.Length; i++) {
          data[i].Add(double.Parse(values[i], CultureInfo.InvariantCulture));
        }
      });
    }

    /// <summary>
    /// Save double lists to text file.
    /// </summary>
    void SaveTextData() {
      if (data == null)
        throw new Exception("No valid data to save.");
      String outfile = outdir + "\\" + timeStamp.ToString("ddMMyyyy-HHmmss") + "-spectrometer.txt";
      for (int i = 0; i < data[0].Count; i++) {
        for (int j = 0; j < data.Length; j++) {
          File.AppendAllText(outfile, data[j][i].ToString("f1", CultureInfo.InvariantCulture) + " ");
        }
        File.AppendAllText(outfile, "\r\n");
      }
    }

    /// <summary>
    /// Save double lists to excel file.
    /// </summary>
    void SaveExcelData() {
      if (data == null)
        throw new Exception("No valid data to save.");
      excelFile = new XlsFile(AppDomain.CurrentDomain.BaseDirectory + "\\template.xls");
      String outfile = timeStamp.ToString("ddMMyyyy-HHmmss") + "-spectrometer.xls";
      excelFile.SetCellValue(1, 1, outfile);
      outfile = outdir + "\\" + outfile;
      for (int i = 0; i < data[0].Count; i++) {
        for (int j = 0; j < data.Length; j++) {
          excelFile.SetCellValue(4 + i, 1 + j, data[j][i]);
        }
      }
      excelFile.Save(outfile);
      Console.WriteLine("Excel saved to: " + outfile);
    }

    void SavePDF() {
      excelFile.PrintToFit = true;
      FlexCel.Render.FlexCelPdfExport flexCelPdfExport1 = new FlexCel.Render.FlexCelPdfExport(excelFile);
      String outfile = outdir + "\\" + timeStamp.ToString("ddMMyyyy-HHmmss") + "-spectrometer.pdf";
      FileStream pdf = new FileStream(outfile, FileMode.Create);
      flexCelPdfExport1.BeginExport(pdf);
      excelFile.ActiveSheet = 1;
      flexCelPdfExport1.ExportSheet();
      flexCelPdfExport1.EndExport();
      pdf.Close();
      Console.WriteLine("PDF saved to: " + outfile);
    }

    /// <summary>
    /// Read serial data from port to 'sData' string until terminator char OR timeout.
    /// </summary>
    void ReadData() {
      Console.WriteLine("Read data started.");
      sData = "";
      DateTime t0 = DateTime.Now;
      while ((DateTime.Now - t0).TotalMilliseconds < timeOutInMs) {
        uint readCount = 0;
        uint byteCount = 0;
        port.GetRxBytesAvailable(ref byteCount);
        if (byteCount > 0) {
          byte[] buf = new byte[byteCount];
          port.Read(buf, byteCount, ref readCount);
          for (int i = 0; i < readCount; i++) {
            // Terminator check
            if (buf[i] == terminator) {
              port.GetRxBytesAvailable(ref byteCount);
              if (byteCount > 0)
                throw new Exception("Buffer not empty after terminator char.");
              Console.WriteLine(sData);
              return;
            }
            // Add to data string
            sData += (char)buf[i];
          }
        }
      }
      throw new Exception("Data read timeout, no terminator char detected.");
    }

    bool IsLinesStatusOk() {
      byte b = 0;
      // Console.WriteLine(port.GetLineStatus(ref b));
      return port.GetLineStatus(ref b) == FTDI.FT_STATUS.FT_OK;
    }

    void StartLineStatusMonitor() {
      new Thread(() => {
        while (IsLinesStatusOk()) {
          Thread.Sleep(1000);
        }
        rxEvent.Set();
      }).Start();
    }

    /// <summary>
    /// Initiates the FTDI port and waits for rx events.
    /// When rx is detected, reads the data and exports it if it is valid.
    /// </summary>
    void ListenData(FTDI.FT_DEVICE_INFO_NODE node) {
      Console.WriteLine("FTDI cable found.");
      port.OpenBySerialNumber(node.SerialNumber);
      port.SetBaudRate(9600);
      port.SetDataCharacteristics(FTDI.FT_DATA_BITS.FT_BITS_7, FTDI.FT_STOP_BITS.FT_STOP_BITS_1, FTDI.FT_PARITY.FT_PARITY_ODD);
      port.SetEventNotification(FTDI.FT_EVENTS.FT_EVENT_RXCHAR, rxEvent);
      StartLineStatusMonitor();
      while (true) {
        Console.WriteLine("Waiting for data...");
        rxEvent.WaitOne();
        if (!IsLinesStatusOk())
          return;
        try {
          ReadData();
          ParseData();
          timeStamp = DateTime.Now;
          //SaveTextData();
          SaveExcelData();
          Console.Beep(2000, 500);
          if (savePDF) {
            SavePDF();
            Console.Beep(2000, 500);
          }
        }
        catch (Exception e2) {
          Console.WriteLine("Data processing exception: " + e2.Message);
        }
        rxEvent.Reset();
      }
    }

    /// <summary>
    /// Listen for a FTDI cable (forever) and, if found, start listening for serial data.
    /// </summary>
    void ListenFTDI() {
      Console.WriteLine("Waiting for FTDI cable...");
      while (true) {
        FTDI.FT_DEVICE_INFO_NODE[] info = new FTDI.FT_DEVICE_INFO_NODE[10];
        port.GetDeviceList(info);
        FTDI.FT_DEVICE_INFO_NODE node = info.ToList().Find(n => n != null && n.SerialNumber == cableSerial);
        if (node != null) {
          ListenData(node);
          Console.WriteLine("FTDI cable lost. Waiting for FTDI cable...");
        }
        // Sleep between checking the ftdi cable presence.
        Thread.Sleep(1000); 
      }
    }

    public static void Main(String[] args) {
      Console.BufferHeight = 5000;
      /*
      XlsFile excelFile = new XlsFile("template.xls");
      excelFile.SetCellValue(1, 1, "JOU");
      excelFile.Save("test1.xls");
      return;
      */
      Program c = new Program();
      if (args.Length > 0)
        c.outdir = args[0];
      if (args.Length > 1)
        c.savePDF = args[1] != "0";
      while(true) {
        try {
          c.ListenFTDI();
        }
        catch (Exception e) {
          Console.WriteLine("Data processing exception: " + e.Message);
        }
      }
    }
  }
}
